import datetime
from django.test import TestCase
from django.test.client import Client
from django.contrib.sites.models import Site
from django.utils import timezone

from kelease.models import Platform, App, Version
from kelease.utils import KeleaseUtils
from kelease.constants import KeleaseK
from kelease.dclasses import UpdateTcDC


class KeleaseTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.site = Site.objects.create(domain='localhost', name='localhost')

        self.platforms = Platform.objects.bulk_create([
            Platform(site=self.site, name='Android', slug='android'),
            Platform(site=self.site, name='IOS', slug='ios')
        ])
        self.apps = App.objects.bulk_create([
            App(site=self.site, app_id='com.example.android', platform=self.platforms[0], download_url='http://www.example.com'),
            App(site=self.site, app_id='com.example.ios', platform=self.platforms[1], download_url='http://www.example.com')
        ])

    def _run_tests(self, test_cases):
        for test_case in test_cases:
            dt = KeleaseUtils().get_update_status(self.site, test_case.inp_platform_slug, test_case.inp_app_id,
                                                  test_case.inp_version_number, last_checked=test_case.inp_last_checked)

            has_current_version = dt['current'] is not None
            has_available_version = dt['available'] is not None

            self.assertEqual(test_case.out_message, dt['action']['message'], msg=test_case.get_test_msg())
            self.assertEqual(test_case.out_should_update, dt['action']['should_update'], msg=test_case.get_test_msg())
            self.assertEqual(test_case.out_can_update, dt['action']['can_update'], msg=test_case.get_test_msg())
            self.assertEqual(test_case.out_version_current, has_current_version, msg=test_case.get_test_msg())
            self.assertEqual(test_case.out_version_available, has_available_version, msg=test_case.get_test_msg())

    def test_get_no_updates(self):
        test_cases = [
            UpdateTcDC(
                test_code='no-updates: 1.0',
                test_message='App not found',
                inp_platform_slug='-', inp_app_id='-', inp_version_number=1,
                out_should_update=False, out_can_update=False, out_version_current=False, out_version_available=False,
                out_message=KeleaseK.UPDATE_MSG_APP_NOT_FOUND.value
            ),
            UpdateTcDC(
                test_code='no-updates: 2.0',
                test_message='No versions',
                inp_platform_slug='android', inp_app_id='com.example.android', inp_version_number=0,
                out_should_update=False, out_can_update=False, out_version_current=False, out_version_available=False,
                out_message=KeleaseK.UPDATE_MSG_NO_VERSIONS.value
            ),
        ]
        self._run_tests(test_cases)

        # Add non current version
        Version.objects.create(site=self.site, app=self.apps[0], version_string='0.1.0', version_number=1,
                               start_date='2020-01-01', is_current=False)

        test_cases = [
            UpdateTcDC(
                test_code='no-updates: 3.0',
                test_message='No current version found',
                inp_platform_slug='android', inp_app_id='com.example.android', inp_version_number=0,
                out_should_update=False, out_can_update=False, out_version_current=False, out_version_available=False,
                out_message=KeleaseK.UPDATE_MSG_NO_VERSIONS.value
            ),
            UpdateTcDC(
                test_code='no-updates: 4.0',
                test_message='Version found, but not current',
                inp_platform_slug='android', inp_app_id='com.example.android', inp_version_number=1,
                out_should_update=False, out_can_update=False, out_version_current=True, out_version_available=False,
                out_message=KeleaseK.UPDATE_MSG_VERSION_FOUND_NOT_CURRENT.value
            )
        ]
        self._run_tests(test_cases)

    def test_get_update_single_version(self):
        Version.objects.create(site=self.site, app=self.apps[0], version_string='0.1.0', version_number=1,
                               start_date='2020-01-01', is_current=True)

        test_cases = [
            UpdateTcDC(
                test_code='single-version: 1.0',
                test_message='Current version only found',
                inp_platform_slug='android', inp_app_id='com.example.android', inp_version_number=1,
                out_should_update=False, out_can_update=False, out_version_current=True, out_version_available=False,
                out_message=KeleaseK.UPDATE_MSG_NO_NEW_UPDATE.value
            ),
            UpdateTcDC(
                test_code='single-version: 2.0',
                test_message='Current version number not found, but update available',
                inp_platform_slug='android', inp_app_id='com.example.android', inp_version_number=0,
                out_should_update=False, out_can_update=True, out_version_current=False, out_version_available=True,
                out_message=KeleaseK.UPDATE_MSG_UPDATE_FOUND.value
            ),
        ]
        self._run_tests(test_cases)

    def test_get_update_multiple_versions(self):
        Version.objects.bulk_create([
            Version(site=self.site, app=self.apps[0], version_string='0.1.0', version_number=1,
                    start_date='2020-01-01', is_current=False),
            Version(site=self.site, app=self.apps[0], version_string='0.1.1', version_number=2,
                    start_date='2020-01-01', is_current=True),
        ])
        test_cases = [
            UpdateTcDC(
                test_code='multiple-versions: 1.0',
                test_message='Current and available versions found',
                inp_platform_slug='android', inp_app_id='com.example.android', inp_version_number=1,
                out_should_update=False, out_can_update=True, out_version_current=True, out_version_available=True,
                out_message=KeleaseK.UPDATE_MSG_UPDATE_FOUND.value
            )
        ]
        self._run_tests(test_cases)

    def test_get_update_required(self):
        Version.objects.bulk_create([
            Version(site=self.site, app=self.apps[0], version_string='0.1.0', version_number=1,
                    start_date='2020-01-01', is_current=False),
            Version(site=self.site, app=self.apps[0], version_string='0.1.1', version_number=2,
                    start_date='2020-01-01', is_current=True, is_required=True),
        ])
        test_cases = [
            UpdateTcDC(
                test_code='multiple-versions: 1.0',
                test_message='Current and available versions found',
                inp_platform_slug='android', inp_app_id='com.example.android', inp_version_number=1,
                out_should_update=True, out_can_update=True, out_version_current=True, out_version_available=True,
                out_message=KeleaseK.UPDATE_MSG_UPDATE_REQUIRED.value
            )
        ]
        self._run_tests(test_cases)

    def test_get_current_expired_new_required(self):
        Version.objects.bulk_create([
            Version(site=self.site, app=self.apps[0], version_string='0.1.0', version_number=1,
                    start_date='2020-01-01', end_date='2020-01-02', is_current=False),
            Version(site=self.site, app=self.apps[0], version_string='0.1.1', version_number=2,
                    start_date='2020-01-01', is_current=True),
        ])
        test_cases = [
            UpdateTcDC(
                test_code='multiple-versions: 1.0',
                test_message='Current and available versions found',
                inp_platform_slug='android', inp_app_id='com.example.android', inp_version_number=1,
                out_should_update=True, out_can_update=True, out_version_current=True, out_version_available=True,
                out_message=KeleaseK.UPDATE_MSG_VERSION_ENDED_UPDATE_REQUIRED.value
            )
        ]
        self._run_tests(test_cases)

    def test_with_last_checked_date(self):
        # Optional update
        Version.objects.bulk_create([
            Version(site=self.site, app=self.apps[0], version_string='0.1.0', version_number=1,
                    start_date='2020-01-01', is_current=False),
            Version(site=self.site, app=self.apps[0], version_string='0.1.1', version_number=2,
                    start_date='2020-01-01', is_current=True),
        ])
        test_cases = [
            UpdateTcDC(
                test_code='with-last-checked-date: 1.0',
                test_message='Update found but last checked too soon',
                inp_platform_slug='android', inp_app_id='com.example.android', inp_version_number=1,
                out_should_update=False, out_can_update=False, out_version_current=True, out_version_available=True,
                out_message=KeleaseK.UPDATE_MSG_UPDATE_CHECK_TOO_SOON.value,
                inp_last_checked=timezone.now() - datetime.timedelta(minutes=15),
            )
        ]
        self._run_tests(test_cases)

        # # Required update
        Version.objects.bulk_create([
            Version(site=self.site, app=self.apps[0], version_string='0.1.2', version_number=3,
                    start_date='2020-01-01', is_current=True, is_required=True),
        ])
        test_cases = [
            UpdateTcDC(
                test_code='with-last-checked-date: 2.0',
                test_message='Update checked too soon but update is required',
                inp_platform_slug='android', inp_app_id='com.example.android', inp_version_number=1,
                out_should_update=True, out_can_update=True, out_version_current=True, out_version_available=True,
                out_message=KeleaseK.UPDATE_MSG_UPDATE_REQUIRED.value,
                inp_last_checked=timezone.now() - datetime.timedelta(minutes=15),
            )
        ]
        self._run_tests(test_cases)