from django.db import models
from django.conf import settings
from django.utils.safestring import mark_safe
from django.urls import reverse


class AbstractModel(models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Platform(AbstractModel):
    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('site', 'slug',)


class Category(AbstractModel):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('site', 'name',)


class App(AbstractModel):
    app_id = models.CharField(max_length=100)
    label = models.CharField(max_length=100, null=True, blank=True)
    code_name = models.CharField(max_length=100, null=True, blank=True)
    download_url = models.URLField(null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    platform = models.ForeignKey(Platform, on_delete=models.CASCADE)
    update_check_auto = models.BooleanField(default=True)
    update_check_delay = models.PositiveIntegerField(default=86400, help_text="In seconds. How much time between "
                                                                              "checking for updates")

    def __str__(self): 
        return f'{self.platform} - {self.app_id}'

    class Meta:
        unique_together = ('site', 'platform', 'app_id')


class Version(AbstractModel):
    app = models.ForeignKey(App, on_delete=models.CASCADE)
    version_string = models.CharField(max_length=50)
    version_number = models.PositiveIntegerField(default=0)
    code_name = models.CharField(max_length=100, null=True, blank=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(null=True, blank=True)
    release_notes = models.TextField(null=True, blank=True)
    file = models.FileField(null=True, blank=True, upload_to="kelease/version/file/")
    url = models.URLField(null=True, blank=True)
    is_current = models.BooleanField(default=False) 
    is_required = models.BooleanField(default=False)

    def __str__(self): 
        return self.version_string 

    class Meta:
        ordering = ('app', '-version_number', '-start_date',)
        unique_together = ('site', 'app', 'version_number')

    def save(self, **kwargs):
        if self.is_current:
            Version.objects.filter(app=self.app, is_current=True).update(is_current=False)
        super().save(**kwargs)

    def get_download_url(self):
        url = (self.file.url if self.file else self.url)
        if not url:
            url = self.app.download_url

        return url
    #
    # def get_download_url(self):
    #     return reverse('kelease:download_version', args=(self.pk,))

    # def display_download_link(self):
    #     html = """
    #         <a href="%s">
    #             <i class="fa fa-download"></i>
    #             Download
    #         </a>
    #     """ % self.get_download_url()
    #     return mark_safe(html)
    # display_download_link.short_description = 'Download'


class Download(AbstractModel):
    app = models.ForeignKey(App, on_delete=models.CASCADE)
    version = models.ForeignKey(Version, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.CASCADE)
    ip_address = models.CharField(max_length=250, null=True, blank=True)

    def __unicode__(self):
        return self.version.version_string

    def save(self, **kwargs):
        self.app = self.version.app
        super().save(**kwargs)
