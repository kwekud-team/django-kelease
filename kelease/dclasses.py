from dataclasses import dataclass
import datetime


@dataclass
class VersionInfo:
    version_number: int
    version_string: str
    download_url: str
    start_date: datetime
    release_notes: str = ''
    end_date: datetime.datetime = None

    def to_json(self):
        if self.version_number:
            return {
                'version_number': self.version_number,
                'version_string': self.version_string,
                'download_url': self.download_url,
                'release_notes': self.release_notes,
                'start_date': self.start_date.strftime('%Y-%m-%d'),
                'end_date': (self.end_date.strftime('%Y-%m-%d') if self.end_date else None)
            }

    @staticmethod
    def none():
        return VersionInfo(version_string='', version_number=0, download_url='', start_date=None)


@dataclass
class UpdateAction:
    should_update: bool
    can_update: bool
    message: str

    def to_json(self):
        return {
            'should_update': self.should_update,
            'can_update': self.can_update,
            'message': self.message,
        }

    @staticmethod
    def none():
        return UpdateAction(should_update=False, can_update=False, message='')


@dataclass
class UpdateInfo:
    current: VersionInfo
    available: VersionInfo
    action: UpdateAction

    def to_json(self):
        return {
            'current': self.current.to_json(),
            'available': self.available.to_json(),
            'action': self.action.to_json(),
        }


@dataclass
class UpdateTcDC:
    test_code: str
    test_message: str

    inp_platform_slug: str
    inp_app_id: str
    inp_version_number: int

    out_should_update: bool
    out_can_update: bool
    out_version_current: bool
    out_version_available: bool
    out_message: str

    inp_last_checked: datetime.datetime = None

    def get_test_msg(self):
        return f'{self.test_code}-{self.test_message}'
