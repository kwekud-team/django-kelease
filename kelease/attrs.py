
class PlatformAttr:
    list_display = ['name', 'slug', 'site']
    list_filter = ['site']


class CategoryAttr:
    list_display = ['name', 'site']
    list_filter = ['site']


class AppAttr:
    list_display = ['app_id', 'label', 'code_name', 'platform', 'category', 'download_url', 'site']
    list_filter = ['site', 'platform', 'category']
    fieldsets = [
        ('Basic',
         {'fields': ('site', 'platform', 'app_id', 'category',)}),
        ('Extra',
         {'fields': ('label', 'code_name', 'download_url', 'update_check_auto', 'update_check_delay', 'is_active',)}),
    ]


class VersionAttr:
    list_display = ['app', 'version_string', 'version_number', 'start_date', 'end_date', 'is_current', 'is_required']
    list_filter = ['start_date', 'end_date', 'is_current', 'is_required']
    fieldsets = [
        ('Basic',
            {'fields': ('site', 'app', 'version_number', 'version_string', 'start_date', 'end_date', )}),
        ('Extra',
            {'fields': ('code_name', 'url', 'file', 'release_notes', 'is_current', 'is_active', 'is_required')}),
    ]


class DownloadAttr:
    list_display = ['version', 'user', 'ip_address', 'date_created']
    list_filter = ('date_created',)
