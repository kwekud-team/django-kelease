from django.utils import timezone

from kelease.models import App
from kelease.constants import KeleaseK
from kelease.dclasses import VersionInfo, UpdateAction, UpdateInfo


class KeleaseUtils:

    def get_update_status(self, site, platform_slug, app_id, version_number, last_checked=None):
        should_update, can_update, message = False, False, ''
        current_version, available_version = None, None

        app = App.objects.filter(site=site, platform__slug__iexact=platform_slug, app_id__iexact=app_id,
                                 is_active=True).first()
        if app:
            now = timezone.now()

            current_version = self._get_current_version(app, version_number)
            available_version = self._get_available_version(app, current_version)

            too_soon = False
            if last_checked:
                seconds_elapsed = (now - last_checked).seconds
                if seconds_elapsed < app.update_check_delay:
                    too_soon = True

            if available_version:
                if current_version and current_version.pk == available_version.pk:
                    can_update = False
                    message = KeleaseK.UPDATE_MSG_NO_NEW_UPDATE.value
                    available_version = None
                elif not available_version.get_download_url():
                    can_update = False
                    message = KeleaseK.UPDATE_MSG_UPDATE_FOUND_NO_DOWNLOAD_URL.value
                    # available_version = None
                else:
                    can_update = True
                    message = KeleaseK.UPDATE_MSG_UPDATE_FOUND.value

                    if available_version.is_required:
                        should_update = True
                        message = KeleaseK.UPDATE_MSG_UPDATE_REQUIRED.value
                    elif current_version and current_version.end_date and current_version.end_date < now:
                        should_update = True
                        message = KeleaseK.UPDATE_MSG_VERSION_ENDED_UPDATE_REQUIRED.value
                    elif too_soon:
                        can_update = False
                        message = KeleaseK.UPDATE_MSG_UPDATE_CHECK_TOO_SOON.value

            else:
                if current_version:
                    message = KeleaseK.UPDATE_MSG_VERSION_FOUND_NOT_CURRENT.value
                else:
                    message = KeleaseK.UPDATE_MSG_NO_VERSIONS.value

        else:
            message = KeleaseK.UPDATE_MSG_APP_NOT_FOUND.value

        update_info = UpdateInfo(
            current=self._get_version_info(current_version),
            available=self._get_version_info(available_version),
            action=UpdateAction(should_update=should_update, can_update=can_update, message=message)
        )

        return update_info.to_json()

    @staticmethod
    def _get_current_version(app, version_number):
        return app.version_set.filter(is_active=True, version_number=version_number).first()

    @staticmethod
    def _get_available_version(app, current_version):
        if current_version and current_version.is_current:
            return current_version
        else:
            return app.version_set.filter(is_active=True, is_current=True).first()

    @staticmethod
    def _get_version_info(version):
        if version:
            return VersionInfo(version.version_number, version.version_string, version.get_download_url(),
                               version.start_date, end_date=version.end_date, release_notes=version.release_notes)
        else:
            return VersionInfo.none()
