from django.http import HttpResponseRedirect
from django.views.generic import View, TemplateView 
from django.contrib.auth import  get_user_model
from django.shortcuts import get_object_or_404
from commons.utils.http import get_client_ip
from core.config.models import Choice
from core.config.utils import get_section
from extend.releases.models import Version, Download


class DownloadCentralView(TemplateView):
    template_name = "releases/download_central.html" 

    def get_context_data(self, **kwargs):
        context = super(DownloadCentralView, self).get_context_data(**kwargs)

        xs = []
        cat_qs = Choice.objects.filter(section=get_section('releases.version-category'))
        for x in cat_qs:
            version_qs = Version.objects.filter(category=x)
            version_qs = version_qs.exclude(is_active=False)
            current_qs = version_qs.filter(is_current=True)
            if current_qs.exists():
                current_version = current_qs[0]
                other_qs = version_qs.exclude(pk=current_version.pk)
            else:
                current_version = None
                other_qs = version_qs.none()

            xs.append({
                'category': x,
                'current_version': current_version,
                'other_qs': other_qs,
            })

        context.update({
            'version_list': xs
        })
        return context


class DownloadVersionView(View):

    def get(self, request, **kwargs):
        url = '/'

        obj = Version.objects.get_or_none(pk=kwargs['pk'])
        if obj:
            user = self.get_user()
            ip_address = get_client_ip(request)
            Download.objects.create(user=user, version=obj, ip_address=ip_address)

            url = obj.get_file_url()

        return HttpResponseRedirect(url)

    def get_user(self):
        user = None

        username = self.request.GET.get('remote_username', None)
        if username:
            qs = get_user_model().objects.filter(username=username)
            user = qs[0] if qs.exists() else None

        if not user:
            user = (self.request.user if self.request.user.is_authenticated else None)

        return user