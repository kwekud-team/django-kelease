from enum import Enum


class KeleaseK(Enum):
    UPDATE_MSG_APP_NOT_FOUND = 'App not found'
    UPDATE_MSG_UPDATE_FOUND = 'Update found'
    UPDATE_MSG_UPDATE_NOT_FOUND = 'No update found'
    UPDATE_MSG_NO_VERSIONS = 'No versions'
    UPDATE_MSG_VERSION_FOUND_NOT_CURRENT = 'Version found but not current'
    UPDATE_MSG_NO_NEW_UPDATE = 'No new update'
    UPDATE_MSG_UPDATE_REQUIRED = 'This update is required'
    UPDATE_MSG_VERSION_ENDED_UPDATE_REQUIRED = 'Current version has expire. Please update now'
    UPDATE_MSG_UPDATE_FOUND_NO_DOWNLOAD_URL = 'Download url not ready'
    UPDATE_MSG_UPDATE_CHECK_TOO_SOON = 'Update check too soon'
