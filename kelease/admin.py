from django.contrib import admin

from kelease.models import Platform, Category, App, Version, Download
from kelease.attrs import PlatformAttr, CategoryAttr, AppAttr, VersionAttr, DownloadAttr


@admin.register(Platform)
class CategoryAdmin(admin.ModelAdmin):
    list_display = PlatformAttr.list_display
    list_filter = PlatformAttr.list_filter
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = CategoryAttr.list_display
    list_filter = CategoryAttr.list_filter


@admin.register(App)
class AppAdmin(admin.ModelAdmin):
    list_display = AppAttr.list_display
    list_filter = AppAttr.list_filter
    fieldsets = AppAttr.fieldsets


@admin.register(Version)
class VersionAdmin(admin.ModelAdmin):
    list_display = VersionAttr.list_display
    list_filter = VersionAttr.list_filter
    fieldsets = VersionAttr.fieldsets


@admin.register(Download)
class DownloadAdmin(admin.ModelAdmin):
    list_display = DownloadAttr.list_display
    list_filter = DownloadAttr.list_filter

