from django.urls import path

from extend.releases import views


app_name = 'releases'

urlpatterns = [
    path('downloads/', views.DownloadCentralView.as_view(), name="download_central"),
    path('download/version/<int:pk>/', views.DownloadVersionView.as_view(), name="download_version"),
]
